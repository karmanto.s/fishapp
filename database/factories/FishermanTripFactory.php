<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FishermanTrip>
 */
class FishermanTripFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    private static $timestamp = 1670147501;
    private static $counter = 3;
    private static $tempCount = 0;

    private static $koordinat = [
                                    [-2.705, 107.6],
                                    [3.765, 98.75],
                                    [5.565, 126.6]
    ];

    public function definition()
    {
        self::$timestamp += 5;
        self::$tempCount = self::$tempCount + 1 >= self::$counter ? 0 : self::$tempCount + 1;

        return [
                'fisherman_id' => self::$tempCount + 1,
                'timestamp' => self::$timestamp,
                'latitude' => self::$koordinat[self::$tempCount][0],
                'longitude' => self::$koordinat[self::$tempCount][1],
                'stat' => 0
        ];
    }
}
