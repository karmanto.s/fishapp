<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fisherman_trips', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('fisherman_id');
            $table->integer('timestamp')->index();
            $table->double('latitude');
            $table->double('longitude');
            $table->tinyInteger('stat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fisherman_trips');
    }
};
