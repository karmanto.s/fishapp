<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('\Database\Seeders\UserSeeder');
        $this->call('\Database\Seeders\GatewaySeeder');
        $this->call('\Database\Seeders\FishermanSeeder');
        $this->call('\Database\Seeders\FishermanTripSeeder');
    }
}

class FishermanTripSeeder extends Seeder
{
    public function run()
    {
        \App\Models\FishermanTrip::factory(10000)->create();
    }
}

class UserSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Admin::create([
            'name' => 'admin',
            'password' => bcrypt('admin')
        ]);
    }
}

class GatewaySeeder extends Seeder
{
    public function run()
    {
        \App\Models\Gateway::create([
            'gateway_id' => '00001',
            'gateway_name' => 'PT Selayar Abadi Sentosa',
            'api_key' => 'db01206b76954ab7b510bb908464fd6d',
            'latitude' => -2.705,
            'longitude' => 107.6
        ]);

        \App\Models\Gateway::create([
            'gateway_id' => '00002',
            'gateway_name' => 'PT Ikan Laut Melimpah',
            'api_key' => '2e78d4cdb8f842eb924c95fb68963ff5',
            'latitude' => 3.765,
            'longitude' => 98.75
        ]);

        \App\Models\Gateway::create([
            'gateway_id' => '00003',
            'gateway_name' => 'PT Ikan Laut Banyak',
            'api_key' => '1f713079b37a4fa28c0a1782a991933f',
            'latitude' => 5.565,
            'longitude' => 126.6
        ]);
    }
}

class FishermanSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Fisherman::create([
            'gateway_id' => 1,
            'fisherman_id' => '00001-00001',
            'fisherman_name' => 'Suhandri'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 1,
            'fisherman_id' => '00001-00002',
            'fisherman_name' => 'Sucahyo'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 1,
            'fisherman_id' => '00001-00003',
            'fisherman_name' => 'ricardo'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 2,
            'fisherman_id' => '00002-00001',
            'fisherman_name' => 'Suhandro'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 2,
            'fisherman_id' => '00002-00002',
            'fisherman_name' => 'albert'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 3,
            'fisherman_id' => '00003-00001',
            'fisherman_name' => 'Asep Abdullah'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 3,
            'fisherman_id' => '00003-00002',
            'fisherman_name' => 'Jhonny'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 3,
            'fisherman_id' => '00003-00003',
            'fisherman_name' => 'Richardo'
        ]);

        \App\Models\Fisherman::create([
            'gateway_id' => 3,
            'fisherman_id' => '00003-00004',
            'fisherman_name' => 'Richard'
        ]);
    }
}

