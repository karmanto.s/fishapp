<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FishermanTrip extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function fisherman()
    {
        return $this->belongsTo(Fisherman::class, 'fisherman_id');
    }
}
