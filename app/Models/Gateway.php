<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'delete_stat'];

    public function fisherman()
    {
        return $this->hasMany(Fisherman::class);
    }
}
