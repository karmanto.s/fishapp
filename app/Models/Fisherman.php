<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fisherman extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'delete_stat', 'last_record'];
    private $result;

    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'gateway_id');
    }

    public function fisherman_trip()
    {
        return $this->hasMany(FishermanTrip::class);
    }

    public function fisherman_trip_timelapse($startTimestamp, $endTimestamp, $opt)
    {
        $this->result = $this->hasMany(FishermanTrip::class)
                        ->where('timestamp', '>=', $startTimestamp)
                        ->where('timestamp', '<=', $endTimestamp);

        switch ($opt) 
        {
            case 1:
                return $this->result
                            ->get();
                break;
            case 2:
                return $this->result
                            ->where('stat', '>=', 1)
                            ->where('stat', '<=', 2)
                            ->get();
                break;
            case 3:
                return $this->result
                            ->where('stat', '=', 1)
                            ->get();
                break;
            case 4:
                return $this->result
                            ->where('stat', '=', 2)
                            ->get();
                break;
            default:
                return $this->result
                            ->get();
        }
    }
}
