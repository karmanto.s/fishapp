<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Fisherman;
use App\Models\FishermanTrip;

class FishermanTripController extends Controller
{
    private $result, $startTimestamp, $endTimestamp, 
            $showOpt, $fishermanId, $lastId, $nowTime, $data;

    public function timelapse(Request $request)
    {
        $this->nowTime = time();

        $this->startTimestamp = (int)$request->startTimestamp > $this->nowTime ? 
                                $this->nowTime : (int)$request->startTimestamp;

        $this->endTimestamp = (int)$request->endTimestamp > $this->nowTime ? 
                                $this->nowTime : (int)$request->endTimestamp;

        $this->showOpt = (int)$request->showOptions;
        $this->fishermanId = $request->fishermanId;

        $this->result = Fisherman::where('fisherman_id', $this->fishermanId)->first()
                        ->fisherman_trip_timelapse($this->startTimestamp, $this->endTimestamp, $this->showOpt);

        $this->data = [
            'result' => $this->result,
            'fishermanId' => $this->fishermanId
        ];

        return response()->json($this->data);
    }

    public function live(Request $request)
    {
        $this->nowTime = time();

        $this->startTimestamp = (int)$request->startTimestamp > $this->nowTime ? 
                                $this->nowTime : (int)$request->startTimestamp;

        $this->endTimestamp = (int)$request->endTimestamp > $this->nowTime ? 
                                $this->nowTime : (int)$request->endTimestamp;

        $this->lastId = (int)$request->lastId;

        $this->result = FishermanTrip::where('id', '>', $this->lastId)
                        ->where('timestamp', '>=', $this->startTimestamp)
                        ->where('timestamp', '<=', $this->endTimestamp)
                        ->get();

        $this->data = [
            'result' => $this->result
        ];

        return response()->json($this->data);
    }
}
