<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Gateway;
use App\Models\Fisherman;

class HomeController extends Controller
{
    public function index() {

        $dataGateway = Gateway::all();
        $dataFisherman = Fisherman::all();

        $data = [
            'dataGateway' => $dataGateway,
            'dataFisherman' => $dataFisherman
        ];

        return view('HomeView', $data);
    }
}
